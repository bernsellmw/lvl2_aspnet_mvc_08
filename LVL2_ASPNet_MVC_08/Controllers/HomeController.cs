﻿using LVL2_ASPNet_MVC_08.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_08.Controllers
{
    public class HomeController : Controller
    {
        db_batch7Entities db = new db_batch7Entities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult GetData(tbl_person person)
        {
            person = db.tbl_person.Where(x => x.Id_Person == 1).SingleOrDefault();
            return Json(person, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Person()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}